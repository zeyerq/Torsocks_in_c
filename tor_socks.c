#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

// gcc tor_socks.c -o tor_socks

int main(int argc, char *argv[]) {
    if (argc < 2) {
        printf("Use: %s [program]\n", argv[0]);
        return 1;
    }

// Sets the value of LD_PRELOAD
    setenv("LD_PRELOAD", "/usr/local/lib/torsocks/libtorsocks.so", 1);

// Run the command provided as an argument
    execvp(argv[1], &argv[1]);

// If execvp fails, display an error message
    perror("Erro ao executar o comando");
    return 1;
}
